var app = new Vue({
  el: '#app',
  data: {
      error_flag: false,
      error_message: 'Error!',
      num_decimales: 2,
      numero_pagas: 12,
      categoria_profesional: "A",
      hijos_en_exclusiva: 0,
      situacion_familiar: "A",
      tipo_contrato_laboral: "0",
      bruto_anual: 30000,
      edad: 40,
      movilidad_geografica: false,
      hijos_menores_25_anos: 0,
      hijos_menores_3_anos: 0,
      hijos_en_exclusiva: false,
      ascendente_mayor_65_menor_75: 0,
      ascendente_mayor_75: 0,
      menor_65_con_discapacidad_cargo: 0,
      numero_personas_deduccion_ascendientes: 0,
      minusvalia_33_al_65: false,
      minusvalia_sup_al_65: false,
      descendientes_con_minusvalia_33_al_65: 0, 
      descendientes_con_minusvalia_sup_al_65: 0,
      ascendientes_con_minusvalia_33_al_65: 0,
      ascendientes_con_minusvalia_sup_al_65: 0,

      cuota_mensual: 0,
      cuota_acumulado_ano: 0,
      rendimiento_neto: 0,
      reduccion_rendimiento_neto: 0,

      sueldo_neto: 0,
      importe_retencion: 0,
      tipo_retencion: 0,
      seguridad_social: 0,
      salario_mensual: 0,
      pagas_extras: 0,
      sueldo_neto_12_pagas: 0
  },
  methods:{
      f_calcular_nomina: function(){
          this.cuota_mensual_pagar = this.f_calcuar_cuota_mensual_pagar();
          this.cuota_acumulado_ano = this.cuota_mensual_pagar * 12;
          this.rendimiento_neto = this.bruto_anual - this.cuota_acumulado_ano;
          this.f_calcular_reduccion_rendimiento_neto();
          this.base_imponible = this.bruto_anual - this.cuota_acumulado_ano - this.reduccion_rendimiento_neto;
          if (0 > this.base_imponible){
              this.base_imponible = 0;
          }

          let suma_minimos = this.f_suma_minimos();

          let cuota_retencion = ( this.f_calcular_tramos_base_liquidable(this.base_imponible) -
                                  this.f_calcular_tramos_base_liquidable(suma_minimos)
                                ).toFixed(this.num_decimales);


          let tipo_previo = (cuota_retencion / this.bruto_anual) * 100;
          let importe_previo_retencion = ( (tipo_previo / 100) * this.bruto_anual ).toFixed(this.num_decimales);
          let deduccion_400_euros = 0;
          let tipo_final_retencion_truncado = this.truncateNumber( ((importe_previo_retencion - deduccion_400_euros) / this.bruto_anual) * 100,
                                                              this.num_decimales );
          tipo_final_retencion_truncado = (0 <= tipo_final_retencion_truncado)?tipo_final_retencion_truncado:0;
                                                              
          let importe_final_retencion = (tipo_final_retencion_truncado / 100) * this.bruto_anual;
          this.seguridad_social = this.cuota_acumulado_ano;
          this.tipo_retencion = this.f_corregir_tipo_retencion(this.f_calcular_tipo_retencion_situacion_contribuyente(tipo_final_retencion_truncado));         
          
          this.importe_retencion = importe_final_retencion.toFixed(2);;
          this.sueldo_neto = (this.bruto_anual - this.seguridad_social - this.importe_retencion).toFixed(2);
          this.sueldo_neto_12_pagas = (this.sueldo_neto / 12).toFixed(2);
          this.pagas_extras = ((this.bruto_anual - this.importe_retencion) / 14).toFixed(2);
          this.salario_mensual = ((this.pagas_extras - this.seguridad_social) / 12).toFixed(2);  

          this.error_flag = false;
      },
      f_calcuar_cuota_mensual_pagar: function(){
          let datos = {
                          A: { min: 1052.9, max: 3751.2 },
                          B: { min: 956.1, max: 3751.2 },
                          C: { min: 831.6, max: 3751.2 },
                          D: { min: 825.6, max: 3751.2 },
                          E: { min: 825.6, max: 3751.2 },
                          F: { min: 825.6, max: 3751.2 },
                          G: { min: 825.6, max: 3751.2 },
                          H: { min: 825.6, max: 3751.2 },
                          I: { min: 825.6, max: 3751.2 },
                          J: { min: 825.6, max: 3751.2 },
                          K: { min: 825.6, max: 3751.2 },
                      };

          let cuota_mensual_pagar = 0;
          let constanteAumento = 0.0635;

          if ( this.bruto_anual / 12 < datos[this.categoria_profesional].min) {
              cuota_mensual_pagar = datos[this.categoria_profesional].min * constanteAumento;
          } else if (this.bruto_anual / 12 > datos[this.categoria_profesional].max) {
              cuota_mensual_pagar = datos[this.categoria_profesional].max * constanteAumento;
          } else {
              cuota_mensual_pagar = (this.bruto_anual / 12) * constanteAumento;
          }

          return cuota_mensual_pagar;
      },
      f_calcular_reduccion_rendimiento_neto: function(){
           let reduccion_comun_todos = 2000;

          this.reduccion_rendimiento_neto = 0;
          if (this.rendimiento_neto < 11250){
              this.reduccion_rendimiento_neto = 3700;
          }
          else if (this.rendimiento_neto >= 14450){
              this.reduccion_rendimiento_neto = 0;
          }
          else{
              this.reduccion_rendimiento_neto = 3700 - 1.15625 * (this.rendimiento_neto - 11250);
          }

          //si el check de movilidad_geografica esta seleccionado
          let incremento_movilidad_geografica = 0
          if (this.movilidad_geografica){
              incremento_movilidad_geografica = this.reduccion_rendimiento_neto;
          }

          let minusvalia_igual_superior_33 = 0;
          if (this.minusvalia_33_al_65){
              minusvalia_igual_superior_33 = 3500;
          }

          let minusvalia_sup_65_o_movilidad_reducida = 0;
          if (this.minusvalia_sup_al_65){
              minusvalia_sup_65_o_movilidad_reducida = 7750;
          }

          //por el momento no tenemos los calculos para desempleados o pensionistas
          let reduccion_desempleado = 0;
          let reduccion_pesionista = 0;

          this.reduccion_rendimiento_neto =    reduccion_comun_todos +
                                              this.reduccion_rendimiento_neto +
                                              incremento_movilidad_geografica +
                                              minusvalia_igual_superior_33 +
                                              minusvalia_sup_65_o_movilidad_reducida +
                                              reduccion_desempleado +
                                              reduccion_pesionista;

          if (2 < this.hijos_menores_25_anos) {
              this.reduccion_rendimiento_neto += 600;
          }
      },
      f_calcular_minimo_personal: function() {
          if (this.edad <= 65) {
              return 5550;
          } else if (this.edad > 75) {
              return 5550 + 918 + 1400;
          } else {
              return 5550 + 1150;
          }
      },
      f_calcular_minimo_descendientes: function() {
          if (this.hijos_menores_25_anos == 0) {
              return 0;
          } else if (this.hijos_menores_25_anos == 1) {
              return 2400;
          } else if (this.hijos_menores_25_anos == 2) {
              return 2400 + 2700;
          } else if (this.hijos_menores_25_anos == 3) {
              return 2400 + 2700 + 4000;
          } else if (this.hijos_menores_25_anos == 4) {
              return 2400 + 2700 + 4000 + 4500;
          } else {
              return (
              2400 +
              2700 +
              4000 +
              4500 +
              4500 * (hijos_menores_25_anos - 4)
              );
          }
      },
      f_suma_minimos: function(){
          let minimo_personal = this.f_calcular_minimo_personal();
          let minimo_descendientes = this.f_calcular_minimo_descendientes();

          let minimo_hijos_beneficiarios = minimo_descendientes;
          if (!this.hijos_en_exclusiva) {
              minimo_hijos_beneficiarios = minimo_descendientes / 2;
          }                

          let minimo_hijos_menores_3_anos = this.hijos_menores_3_anos * 2800;

          let minimo_hijos_menores_3_anos_beneficiarios = minimo_hijos_menores_3_anos;
          if (!hijos_en_exclusiva) {
              minimo_hijos_menores_3_anos_beneficiarios = minimo_hijos_menores_3_anos / 2;
          }

          let divisor_para_minimos_deduccion_ascendientes = 1;
          if (this.numero_personas_deduccion_ascendientes > 0){
              divisor_para_minimos_deduccion_ascendientes = this.numero_personas_deduccion_ascendientes;
          }

          let minimo_ascendente_mayor_65_menor_75 = (this.ascendente_mayor_65_menor_75 * 1150) / divisor_para_minimos_deduccion_ascendientes;
          let minimo_ascendente_mayor_75 = (this.ascendente_mayor_75 * 2550) / divisor_para_minimos_deduccion_ascendientes;
          let minimo_menor_65_con_discapacidad_cargo = (this.menor_65_con_discapacidad_cargo * 1150) / divisor_para_minimos_deduccion_ascendientes;

          let minimo_descendientes_con_minusvalia_33_al_65 = this.descendientes_con_minusvalia_33_al_65 * 3000;
          let minimo_descendientes_con_minusvalia_33_al_65_beneficiarios = minimo_descendientes_con_minusvalia_33_al_65;
          if (!this.hijos_en_exclusiva) {
              minimo_descendientes_con_minusvalia_33_al_65_beneficiarios = minimo_descendientes_con_minusvalia_33_al_65 / 2;
          }

          let minimo_descendientes_con_minusvalia_sup_al_65 = this.descendientes_con_minusvalia_sup_al_65 * 12000;
          let minimo_descendientes_con_minusvalia_sup_al_65_beneficiarios = minimo_descendientes_con_minusvalia_sup_al_65;                
          if (!this.hijos_en_exclusiva) {
              minimo_descendientes_con_minusvalia_sup_al_65_beneficiarios = minimo_descendientes_con_minusvalia_sup_al_65 / 2;
          }

          let minimo_ascendientes_con_minusvalia_33_al_65 = (this.ascendientes_con_minusvalia_33_al_65 * 3000) / divisor_para_minimos_deduccion_ascendientes;
          let minimo_ascendientes_con_minusvalia_sup_al_65 = (this.ascendientes_con_minusvalia_sup_al_65 * 12000) / divisor_para_minimos_deduccion_ascendientes;

          let minimo_minusvalia_33_al_65 = 0;
          if (this.minusvalia_33_al_65) {
              minimo_minusvalia_33_al_65 = 3000;
          }

          let minimo_minusvalia_sup_al_65 = 0;
          if (this.minusvalia_sup_al_65) {
              minimo_minusvalia_sup_al_65 = 12000;
          }

          let suma_minimos =  minimo_personal +
                              minimo_hijos_beneficiarios +
                              minimo_hijos_menores_3_anos_beneficiarios +
                              minimo_ascendente_mayor_65_menor_75 +
                              minimo_ascendente_mayor_75 +
                              minimo_menor_65_con_discapacidad_cargo +
                              minimo_descendientes_con_minusvalia_33_al_65_beneficiarios +
                              minimo_descendientes_con_minusvalia_sup_al_65_beneficiarios +
                              minimo_ascendientes_con_minusvalia_33_al_65 +
                              minimo_ascendientes_con_minusvalia_sup_al_65 +
                              minimo_minusvalia_33_al_65 +
                              minimo_minusvalia_sup_al_65;

          return suma_minimos;
      },
      f_calcular_tipo_retencion_situacion_contribuyente:  function(tipo_retencion){
          let _tipo_retencion = 0;

          if (this.situacion_familiar == "A") {
              if (this.hijos_menores_25_anos == 0) 
                  return tipo_retencion;
              else if (this.hijos_menores_25_anos == 1) {
                  if (this.bruto_anual <= 14266) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              } 
              else {
                  if (this.bruto_anual <= 15803) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              }
          } 
          else if (this.situacion_familiar == "B") {
              if (this.hijos_menores_25_anos == 0) {
                  if (this.bruto_anual <= 13696) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              } 
              else if (this.hijos_menores_25_anos == 1) {
                  if (this.bruto_anual <= 14985) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              } else {
                  if (this.bruto_anual <= 17138) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
          }
          } 
          else if (this.situacion_familiar == "C") {
              if (this.hijos_menores_25_anos == 0) {
                  if (this.bruto_anual <= 12000) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              } 
              else if (this.hijos_menores_25_anos == 1) {
                  if (this.bruto_anual <= 12607) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              } 
              else {
                  if (this.bruto_anual <= 13275) 
                      return _tipo_retencion;
                  else 
                      return tipo_retencion;
              }
          }
      },
      f_corregir_tipo_retencion: function(tipo_retencion){
          let isCorregirTipoContratoLaboral = "1" == this.tipo_contrato_laboral && 2 > tipo_retencion;
          if (isCorregirTipoContratoLaboral){
              tipo_retencion = 2;
          }
          tipo_retencion = (0 > tipo_retencion)?0:tipo_retencion;
          return tipo_retencion;
      },
      f_calcular_tramos_base_liquidable: function(base_liquidable) {
          // tramo 1
          let tramo_1 = (base_liquidable < 12450)? base_liquidable * 0.19 : 12450 * 0.19;

          //tramo 2, si hay exceso en el tramo
          let tramo_2 = 0
          if (base_liquidable >= 12450  ) {
              tramo_2 = (base_liquidable > 20200)?(20200 - 12450) * 0.24:(base_liquidable - 12450) * 0.24;
          }

          //tramo 3, si hay exceso en el tramo
          let tramo_3 = 0;
          if (base_liquidable >= 20200) {
              tramo_3 = (base_liquidable > 35200)?(35200 - 20200) * 0.3:(base_liquidable - 20200) * 0.3;
          }

          //tramo 4, si hay exceso en el tramo
          let tramo_4 = 0;
          if (base_liquidable >= 35200) {
              tramo_4 =  (base_liquidable > 60000) ? (60000 - 35200) * 0.37 : (base_liquidable - 35200) * 0.37;
          }

          //tramo 5, ultimo tramo
          let tramo_5 = 0;
          if (base_liquidable >= 60000) {
              tramo_5 = (base_liquidable - 60000) * 0.45;
          }

          //devuelvo el sumatorio de todos los tramos!!!!
          return tramo_1 + tramo_2 + tramo_3 + tramo_4 + tramo_5;
      },
      /**
       * Truncar numeros
       * 
       * @param {*} numero 
       * @param {*} decimales 
       */
      truncateNumber: function(numero, decimales){
          decimales = decimales * 10;
          numero = numero * decimales;
          resultado = Math.trunc(numero);
          resultado = resultado / decimales;
          return resultado;
      },
  }
});